import Vue from "vue";
import Vuex from "vuex";

import auth from "./auth";
import trials from "./trials";
import locations from "./locations";
import calendars from "./calendars";
import students from "./students";
import coaches from "./coaches";
import fees from "./fees";
import onlineClasses from "./onlineclasses";
import payments from "./payments";
import credits from "./credits";
import classgroup from "./classgroup";
import users from "./users";
import leads from "./leads";
import ccleads from "./ccleads";
import accounts from "./accounts";
import cms from "./cms";
import classes from "./classes";
import orders from "./orders";
import notifications from "./notifications";
import trainingPlans from "./trainingPlans";
import attendances from "./attendances";
import assessments from "./assessments";
Vue.use(Vuex);

const store = new Vuex.Store({
  modules: {
    auth,
    trials,
    locations,
    calendars,
    attendances,
    students,
    coaches,
    fees,
    onlineClasses,
    payments,
    credits,
    classgroup,
    users,
    leads,
    ccleads,
    accounts,
    cms,
    classes,
    orders,
    notifications,
    trainingPlans,
    assessments
  }
});

export default store;
