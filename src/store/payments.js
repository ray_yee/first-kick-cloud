import { db, fs } from "../boot/firebase";
import moment from "moment";
import orderBy from 'lodash/orderBy'

export default {
  namespaced: true,

  state: {
    list: null,
    pastlist: null,
    studentPayments: null,
    paid: null,
    unpaid: null,
    paidPayNow: null,
    unpaidPayNow: null,
    payNowByKey:null
  },

  mutations: {
    SET_PAYMENTS(state, payload) {
      state.list = payload;
    },
    SET_PAST_PAYMENTS(state, payload) {
      state.pastlist = payload;
    },
    SET_STUDENT_PAYMENTS(state, payload) {
      state.studentPayments = orderBy(payload, 'date');
    },
    SET_ALL_PAYMENTS(state, payload) {
      let paid= [], unpaid=[]
      Object.keys(payload).forEach(key=> {
        const {paymentStatus} = payload[key]
        if (paymentStatus === 'Paid') {
          paid.push({
            ...payload[key],
            key
          })
        }
        else {
          unpaid.push({
            ...payload[key],
            key
          })
        }
      })
      state.paid= paid
      state.unpaid = unpaid
    },
    SET_ALL_PAYNOW(state, payload) {
      let paid= [], unpaid=[]
      state.payNowByKey = payload
      Object.keys(payload).forEach(key=> {
        const {status} = payload[key]
        if (status === 'Paid') {
          paid.push({
            ...payload[key],
            key
          })
        }
        else {
          unpaid.push({
            ...payload[key],
            key
          })
        }
      })
      state.paidPayNow= paid
      state.unpaidPayNow = unpaid
    }
  },

  actions: {
    loadAllPayNow({ commit }) {
      return new Promise((resolve, reject) => {
        fs.collection("payments")
        .where("status", "==", 'Paid')
          .onSnapshot(function(querySnapshot) {
            let payments = {};
            querySnapshot.forEach(function(doc) {
              payments[doc.id] = {
                ...doc.data()
              };
            });
            commit("SET_ALL_PAYNOW", payments);
            resolve(payments);
          })
          
      });
    },
    loadAllPayments({ commit }) {
      return new Promise((resolve, reject) => {
        fs.collection("paymentAdvices")
          .onSnapshot(function(querySnapshot) {
            let payments = {};
            querySnapshot.forEach(function(doc) {
              payments[doc.id] = {
                ...doc.data()
              };
            });
            commit("SET_ALL_PAYMENTS", payments);
            resolve(payments);
          })
          
      });
    },
    loadPaymentsByMonthYear({commit}, {month, year}) {
      return new Promise(async (resolve, reject) => {
        console.log(month, year)
        let payments = {};

            fs.collection("paymentAdvices")
          .where("year", "==", year)
          .where("month", "==", month)
          .where("type", "==", "Term Fees")
          .onSnapshot(function(querySnapshot) {
            
            querySnapshot.forEach(function(doc) {
              payments[doc.id] = {
                ...doc.data()
              };
            });
            commit("SET_PAYMENTS", payments);
        resolve(payments);
    
          })
          }) 
      
    },
    loadPastPayments({ commit }, yearList) {
      return new Promise(async (resolve, reject) => {
        let payments = {};
        let promises = Object.keys(yearList).map(year=> {
          return new Promise((res, rej) => {
            fs.collection("paymentAdvices")
          .where("year", "==", year)
          .where("month", "in", yearList[year])
          .where("type", "==", "Term Fees")
          .onSnapshot(function(querySnapshot) {
            
            querySnapshot.forEach(function(doc) {
              payments[doc.id] = {
                ...doc.data()
              };
            });
            res()
          })
          })
          
        })
        await Promise.all(promises)
        commit("SET_PAST_PAYMENTS", payments);
        resolve(payments);
        
          
      });
    },
    loadPayments({ commit }, yearList) {
      return new Promise(async (resolve, reject) => {
        let payments = {};
        let promises = Object.keys(yearList).map(year=> {
          return new Promise((res, rej) => {
            fs.collection("paymentAdvices")
          .where("year", "==", year)
          .where("month", "in", yearList[year])
          .where("type", "==", "Term Fees")
          .onSnapshot(function(querySnapshot) {
            
            querySnapshot.forEach(function(doc) {
              payments[doc.id] = {
                ...doc.data()
              };
            });
            res()
          })
          })
          
        })
        await Promise.all(promises)
        commit("SET_PAYMENTS", payments);
        resolve(payments);
        
          
      });
    },
    loadStudentPayments({ commit }, studentKey) {
      return new Promise((resolve, reject) => {
        fs.collection("paymentAdvices")
        .where('studentKey', '==', studentKey)
          .get().then(function(querySnapshot) {
            let results = [];

            querySnapshot.forEach(function(doc) {
              results.push({
                ...doc.data(),
                key: doc.id
              });
            });
            commit("SET_STUDENT_PAYMENTS", results.reverse());
            resolve(results);
          })
          
          
        
      });
    },
    addBulkPayments({ commit }, { data, students }) {
      return new Promise(async (resolve, reject) => {
        let batch = fs.batch();
        students.forEach(student => {
          let newRef = fs.collection("paymentAdvices").doc();
          batch.set(newRef, {
            ...data,
            email: student.email.toLowerCase().trim(),
            childName: student.childName,
            studentKey: student.key
          });
        });
        batch
          .commit()
          .then(() => {
            resolve();
          })
          .catch(err => {
            console.log(err);
            resolve();
          });
      });
    },
    addPayment({ commit }, data) {
      return new Promise(async (resolve, reject) => {
        await fs.collection("paymentAdvices").add(data);
        resolve();
      });
    },
    updatePayment({ commit }, { data, key }) {
      return new Promise(async (resolve, reject) => {
        await fs.collection("paymentAdvices").doc(key).update(data);
        resolve();
      });
    },
    deletePayment({ commit }, key) {
      return fs.collection("paymentAdvices").doc(key).delete();
    },
    copyPayment({ commit }, data) {
      return new Promise((resolve, reject) => {
        fs.collection(`${moment(data.date).format("YYYY")}payments`)
          .add(data)
          .then(function() {
            console.log("Document successfully written!");
            resolve();
          })
          .catch(function(error) {
            console.log(data);
            console.error("Error writing document: ", error);

            reject(error);
          });
      });
    },
    deletePayNow({commit}, key) {
      return fs.collection("payments").doc(key).delete();

    },
    addPayNow({ commit }, data) {
      return new Promise(async (resolve, reject) => {
       fs.collection("payments").add(data).then((docRef) => {
          resolve(docRef.id);
      }).catch(error => console.log(error))
        
      });
    },

    updatePayNow({ commit }, { data, key }) {
      return new Promise(async (resolve, reject) => {
        await fs.collection("payments").doc(key).update(data);
        resolve();
      });
    },

   
  }
};
