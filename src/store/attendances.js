import {
  fs
} from "../boot/firebase";
import orderBy from "lodash/orderBy";
import moment from 'moment'

export default {
  namespaced: true,

  state: {
    list: null,
    trialAttendance: null,
    coachAttendance: null,
    coachMonthAttendance: null,
    adminAttendance: null
  },

  mutations: {
    SET_ATTENDANCES(state, payload) {
      state.list = payload;
    },
    SET_TRIAL_ATTENDANCE(state, payload) {
      state.trialAttendance = payload;
    },
    SET_COACH_ATTENDANCE(state, payload) {
      state.coachAttendance = payload;
    },
    SET_COACH_MONTH_ATTENDANCE(state, payload) {
      state.coachMonthAttendance = payload;
    },
    ADD_ATTENDANCE(state, {
      data,
      newRef
    }) {
      state.attendance = {
        ...state.attendance,
        [data.studentKey]: {
          ...data,
          key: newRef.id
        }
      };
    },
    ADD_TRIAL_ATTENDANCE(state, {
      data,
      newRef
    }) {
      state.trialAttendance = {
        ...state.trialAttendance,
        [data.trialKey]: {
          ...data,
          key: newRef.id
        }
      };
    },
    ADD_COACH_ATTENDANCE(state, {
      classKey,
      data
    }) {
      let coachAttendance = {
        ...state.coachAttendance
      };
      if (coachAttendance[classKey] === undefined) {
        coachAttendance[classKey] = [];
      }
      coachAttendance[classKey].push(data);
      state.coachAttendance = coachAttendance;
    },
    ADD_ADMIN_ATTENDANCE(state, data) {
      state.adminAttendance.push(data);
    },
    REMOVE_ATTENDANCE(state, studentKey) {
      let newAttendance = {
        ...state.attendance
      };
      delete newAttendance[studentKey];
      state.attendance = newAttendance;
    },
    REMOVE_TRIAL_ATTENDANCE(state, trialKey) {
      let newAttendance = {
        ...state.trialAttendance
      };
      delete newAttendance[trialKey];
      state.trialAttendance = newAttendance;
    },
    REMOVE_COACH_ATTENDANCE(state, {
      classKey,
      index
    }) {
      let coachAttendance = {
        ...state.coachAttendance
      };
      coachAttendance[classKey].splice(index, 1);
      state.coachAttendance = coachAttendance;
    },
    REMOVE_ADMIN_ATTENDANCE(state, index) {
      state.adminAttendance.splice(index, 1);
    }
  },

  actions: {
    loadAttendances({
      commit
    }, {
      date,
      location
    }) {
      return new Promise((resolve, reject) => {
        fs.collection("attendances")
          .where("location", "==", location)
          .where("date", "==", date)
          .onSnapshot(function (querySnapshot) {
            let attendances = {};
            querySnapshot.forEach(function (doc) {
              const {
                studentKey
              } = doc.data();
              attendances[studentKey] = {
                ...doc.data(),
                key: doc.id
              };
            });
            commit("SET_ATTENDANCES", attendances);
            resolve(attendances);
          });
      });
    },
    loadAttendancesByMonthYear({
      commit
    }, {
      month,
      year,
      location
    }) {
      return new Promise((resolve, reject) => {
        fs.collection("attendances")
          .where("location", "==", location)
          .where("month", "==", month)
          .where('year', '==', year)
          .onSnapshot(function (querySnapshot) {
            let attendances = {};
            querySnapshot.forEach(function (doc) {
              const {
                studentKey,
                date
              } = doc.data();
              if (attendances[studentKey]) {
                attendances[studentKey].push(date)
              } else {
                attendances[studentKey] = [date]
              }
            });
            commit("SET_ATTENDANCES", attendances);
            resolve(attendances);
          });
      });
    },
    addAttendance({
      commit
    }, data) {
      return new Promise(async resolve => {
        const newRef = await fs.collection("attendances").add(data);
        resolve(newRef);
      });
    },
    removeAttendance({
      commit
    }, key) {
      return new Promise(async resolve => {
        await fs
          .collection("attendances")
          .doc(key)
          .delete();
        resolve();
      });
    },

    loadTrialAttendance({
      commit
    }, {
      date,
      locationId
    }) {
      return new Promise((resolve, reject) => {
        fs.collection("trialAttendance")
          .where("locationId", "==", locationId)
          .where("date", "==", date)
          .get()
          .then(function (querySnapshot) {
            let attendance = {};
            querySnapshot.forEach(function (doc) {
              const {
                trialKey
              } = doc.data();
              attendance[trialKey] = {
                ...doc.data(),
                key: doc.id
              };
            });
            commit("SET_TRIAL_ATTENDANCE", attendance);
            resolve(attendance);
          })
          .catch(function (error) {
            reject();
            console.log("Error getting documents: ", error);
          });
      });
    },

    addTrialAttendance({
      commit
    }, data) {
      return new Promise(async resolve => {
        const newRef = await fs.collection("trialAttendance").add(data);
        commit("ADD_TRIAL_ATTENDANCE", {
          data,
          newRef
        });
        resolve(newRef);
      });
    },
    removeTrialAttendance({
      commit
    }, {
      key,
      trialKey
    }) {
      return new Promise(async resolve => {
        await fs
          .collection("trialAttendance")
          .doc(key)
          .delete();
        commit("REMOVE_TRIAL_ATTENDANCE", trialKey);
        resolve();
      });
    },

    loadCoachAttendance({
      commit
    }, {
      date,
      location
    }) {
      return new Promise((resolve, reject) => {
        fs.collection("coachAttendances")
          .where("location", "==", location)
          .where("date", "==", date)
          .onSnapshot(function (querySnapshot) {
            let todayAttendance = {};
            querySnapshot.forEach(function (doc) {
              const coach = doc.data();
              if (todayAttendance[coach.classKey] === undefined) {
                todayAttendance[coach.classKey] = [];
              }
              todayAttendance[coach.classKey].push({
                ...doc.data(),
                key: doc.id
              });
            });
            commit("SET_COACH_ATTENDANCE", todayAttendance);
            resolve(todayAttendance);
          })

      });
    },
    loadAllCoachAttendance({commit}) {
      return new Promise((resolve, reject) => {
        fs.collection("coachAttendances")
          .get().then(function (querySnapshot) {
            let attendance = {};
           
            querySnapshot.forEach(function (doc) {
              const coach = doc.data();
              const caRef = fs.collection("coachAttendances").doc(doc.id)
              let dateSplit = coach.date.split('-')
              let month = moment().month(parseInt(dateSplit[1])-1).format('MMMM')
              let year = dateSplit[2]
              attendance[doc.id] = { 
                ...coach,
                month, year
              }
             caRef.update({...coach, month, year})
            });
            commit("SET_COACH_ATTENDANCE", attendance);
            resolve(attendance);
          })

      });
    },
    loadCoachAttendanceByMonthYear({commit}, {month, year, location}) {
      return new Promise((resolve, reject) => {
        fs.collection("coachAttendances")
        .where("location", "==", location)
        .where("month", "==", month)
        .where("year", "==", year)
          .onSnapshot(function (querySnapshot) {
            let attendance = {};
            querySnapshot.forEach(function (doc) {
              const coach = doc.data();
              attendance[doc.id] = { 
                ...coach,
                month, year
              }
            });
            commit("SET_COACH_ATTENDANCE", attendance);
            resolve(attendance);
          })

      });
    },
    loadCoachAttendanceByDates({
      commit
    }, {
      dates,
      location
    }) {
      return new Promise((resolve, reject) => {
        const coachAttendancePath = fs.collection("coachAttendances")
        let batches = []

        while (dates.length) {
          // firestore limits batches to 10
          const batch = dates.splice(0, 10);

          // add the batch request to to a queue
          batches.push(
            new Promise(response => {
              coachAttendancePath
                .where("location", "==", location)
                .where("date", "in", batch)
                .get()
                .then(results => response(results.docs.map(result => ({
                  ...result.data(),
                  key: result.id
                }))))
            })
          )
        }
        Promise.all(batches).then(content => {
          let coachAttendance = content.flat();
          coachAttendance = orderBy(coachAttendance, ["date"]);
          let groupedCoachAttendance = {};
          coachAttendance.map(att => {
            if (groupedCoachAttendance[att.coachKey] === undefined) {
              groupedCoachAttendance[att.coachKey] = {};
            }
            if (
              groupedCoachAttendance[att.coachKey][att.date] === undefined
            ) {
              groupedCoachAttendance[att.coachKey][att.date] = [];
            }
            groupedCoachAttendance[att.coachKey][att.date].push(att);
          });

          if (Object.keys(groupedCoachAttendance).length > 0) {
            commit("SET_COACH_MONTH_ATTENDANCE", groupedCoachAttendance);
          } else {
            commit("SET_COACH_MONTH_ATTENDANCE", null);
          }
          resolve(coachAttendance);
        })

      })


    },
    addCoachAttendance({
      commit
    }, data) {
      return fs.collection("coachAttendances").add(data);

    },

    removeCoachAttendance({
      commit
    }, key) {
      return fs
        .collection("coachAttendances")
        .doc(key)
        .delete();

    }
  }
};
