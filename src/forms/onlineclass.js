export const formConfig = [
  {
    label: "Class Name",
    fieldName: "name",
    type: "Input"
  },
  {
    label: "Day of Week",
    fieldName: "dayOfWeek",
    type: "Select",
    listName: null,
    values: [
      "Monday",
      "Tuesday",
      "Wednesday",
      "Thursday",
      "Friday",
      "Saturday",
      "Sunday"
    ]
  },
  {
    label: "Start Time",
    fieldName: "startTime",
    type: "TimePicker"
  },
  {
    label: "End Time",
    fieldName: "endTime",
    type: "TimePicker"
  },
  {
    label: "Active",
    fieldName: "active",
    type: "Toggle"
  }
];
