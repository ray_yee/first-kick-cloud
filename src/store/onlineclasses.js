import { db } from "../boot/firebase";

export default {
  namespaced: true,

  state: {
    onlineClasses: []
  },
  getters: {
    onlineClasses(state) {
      return state.onlineClasses;
    }
  },

  mutations: {
    SET_ONLINE_CLASSES(state, payload) {
      state.onlineClasses = payload;
    },
    ADD_ONLINE_CLASSES(state, payload) {
      state.onlineClasses.push(payload);
    }
  },

  actions: {
    loadOnlineClasses({ commit }) {
      return new Promise((resolve, reject) => {
        db.ref("onlineclasses")
          .once("value", snapshot => {
            let val = snapshot.val();
            if (val) {
              let onlineclasses = [];
              Object.keys(val).forEach(key => {
                onlineclasses.push({
                  ...val[key],
                  key
                });
              });
              commit("SET_ONLINE_CLASSES", onlineclasses);
              resolve(onlineclasses);
            }
            resolve();
          })
          .catch(function(error) {
            reject();
            console.log("Error getting documents: ", error);
          });
      });
    },
    addOnlineClasses({ commit }, data) {
      return new Promise(async (resolve, reject) => {
        const newRef = await db.ref("onlineclasses").push(data);
        commit("ADD_ONLINE_CLASSES", { ...data, key: newRef.key });
        resolve();
      });
    }
  }
};
