export const formConfig = [
    {
      label: "Coach's Name",
      fieldName: "name",
      type: "Input"
    },
    {
      label: "Date of Birth (YYYY-MM-DD)",
      fieldName: "dateOfBirth",
      type: "DatePicker"
    },
    {
      label: "Payment Rate",
      fieldName: "paymentRate",
      type: "Select",
    listName: null,
    required: false,
    values: ["OJT", "30", "40","50","60","70", "80"]
    },
    {
      label: "Short Name",
      fieldName: "shortName",
      type: "Input"
    },
    {
      label: "Email",
      fieldName: "email",
      type: "Input"
    },
    {
      label: "Mobile Number",
      fieldName: "contact",
      type: "Input"
    }
  ];
  