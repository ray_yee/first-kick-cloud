const routes = [
  {
    path: "/login",
    name: "login",
    component: () => import("pages/Login")
  },
  {
    path: "/",
    component: () => import("layouts/allLocation"),
    children: [
      {
        path: "/",
        name: "home",
        component: () => import("pages/Index"),
        meta: {
          requiresAuth: true
        }
      },
      {
        path: "/orders",
        name: "Orders",
        component: () => import("pages/Orders"),
        meta: {
          requiresAuth: true
        }
      },
      {
        path: "/paynow",
        name: "PayNow",
        component: () => import("pages/PayNow"),
        meta: {
          requiresAuth: true
        }
      },
      {
        path: "/credits",
        name: "Credits",
        component: () => import("pages/Credits"),
        meta: {
          requiresAuth: true
        }
      },
      {
        path: "/coachingcourse",
        name: "Coaching Course",
        component: () => import("pages/CoachingCourse"),
        meta: {
          requiresAuth: true
        }
      },
      {
        path: "/coaches",
        name: "Coaches",
        component: () => import("pages/Coaches"),
        meta: {
          requiresAuth: true
        }
      },
      {
        path: "/locations",
        name: "Locations",
        component: () => import("pages/Locations"),
        meta: {
          requiresAuth: true
        }
      },

      {
        path: "students",
        name: "students",
        component: () => import("pages/Students"),
        meta: {
          requiresAuth: true
        }
      },
      {
        path: "students/details",
        name: "student details",
        component: () => import("pages/Students/Details"),
        meta: {
          requiresAuth: true
        }
      },

      {
        path: "upload-students",
        name: "uploadstudents",
        component: () => import("pages/uploadStudents"),
        meta: {
          requiresAuth: true
        }
      },
      {
        path: "/users",
        name: "Users",
        component: () => import("pages/Users"),
        meta: {
          requiresAuth: true
        }
      },
      {
        path: "accounts",
        name: "accounts",
        component: () => import("pages/Accounts"),
        meta: {
          requiresAuth: true
        }
      },
      {
        path: "/fees-settings",
        name: "Manage Fees",
        component: () => import("pages/Fees"),
        meta: {
          requiresAuth: true
        }
      },
      {
        path: "/training-plans",
        name: "Training Plans",
        component: () => import("pages/TrainingPlans"),
        meta: {
          requiresAuth: true
        }
      }
    ]
  },
  {
    path: "/cms",
    component: () => import("layouts/allLocation"),
    children: [
      {
        path: "locations",
        name: "CMS Locations",
        component: () => import("pages/CMS/Locations"),
        meta: {
          requiresAuth: true
        }
      },
      {
        path: "pages",
        name: "Pages",
        component: () => import("pages/CMS/Pages"),
        meta: {
          requiresAuth: true
        }
      },
      {
        path: "blogs",
        name: "Blogs",
        component: () => import("pages/CMS/Blogs"),
        meta: {
          requiresAuth: true
        }
      },
      {
        path: "news",
        name: "News",
        component: () => import("pages/CMS/News"),
        meta: {
          requiresAuth: true
        }
      },
      {
        path: "faqs",
        name: "FAQs",
        component: () => import("pages/CMS/FAQs"),
        meta: {
          requiresAuth: true
        }
      },
      
    ]
  },

  {
    path: "/location",
    component: () => import("layouts/default"),
    children: [
      {
        path: "trials",
        name: "trials",
        component: () => import("pages/Trials"),
        meta: {
          requiresAuth: true
        }
      },
      {
        path: "payments",
        name: "payments",
        component: () => import("pages/Payments"),
        meta: {
          requiresAuth: true
        }
      },
      {
        path: "paymentadvices",
        name: "Payment Advices",
        component: () => import("pages/PaymentAdvices"),
        meta: {
          requiresAuth: true
        }
      },
      {
        path: "attendance",
        name: "attendance",
        component: () => import("pages/Attendance"),
        meta: {
          requiresAuth: true
        }
      },
      {
        path: "attendance/summary",
        name: "attendance summary",
        component: () => import("pages/Attendance/Summary"),
        meta: {
          requiresAuth: true
        }
      },
      {
        path: "archived",
        name: "archived",
        component: () => import("pages/Archived"),
        meta: {
          requiresAuth: true
        }
      },
      // {
      //   path: "payments",
      //   name: "payments",
      //   component: () => import("pages/Payments"),
      //   meta: {
      //     requiresAuth: true
      //   }
      // },
      {
        path: "leads",
        name: "Leads",
        component: () => import("pages/Leads"),
        meta: {
          requiresAuth: true
        }
      },
      {
        path: "/copyclasses",
        name: "copyclasses",
        component: () => import("pages/CopyClasses"),
        meta: {
          requiresAuth: true
        }
      },
      {
        path: "/copycredits",
        name: "copycredits",
        component: () => import("pages/CopyCredits"),
        meta: {
          requiresAuth: true
        }
      },
      {
        path: "reports/leads-conversion",
        name: "Leads Conversion",
        component: () => import("pages/Reports/LeadsConversionReport"),
        meta: {
          requiresAuth: true
        }
      },
      {
        path: "reports/coaches-pay",
        name: "Coaches Pay",
        component: () => import("pages/Reports/CoachesPayReport"),
        meta: {
          requiresAuth: true
        }
      },
      {
        path: "/classes",
        name: "Classes",
        component: () => import("pages/Classes"),
        meta: {
          requiresAuth: true
        }
      },
      {
        path: "monthly-classes",
        name: "Monthly Classes",
        component: () => import("pages/MonthlyClasses"),
        meta: {
          requiresAuth: true
        }
      }
    ]
  }
];

// Always leave this as last one
if (process.env.MODE !== "ssr") {
  routes.push({
    path: "*",
    component: () => import("pages/Error404.vue")
  });
}

export default routes;
