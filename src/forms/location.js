export const formConfig = [
  {
    label: "Location Name",
    fieldName: "name",
    type: "Input"
  },
  {
    label: "Address",
    fieldName: "address",
    type: "TextArea"
  },
  {
    label: "Logo URL",
    fieldName: "logoURL",
    type: "TextArea"
  },
  {
    label: "Location Id",
    fieldName: "id",
    type: "Input"
  },
  {
    label: "Max Class Size",
    fieldName: "maxClassSize",
    type: "Input"
  }
];
