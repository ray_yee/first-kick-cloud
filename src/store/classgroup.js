import { fs } from "../boot/firebase";

export default {
  namespaced: true,

  state: {
    monthlyClasses: null
  },

  mutations: {
    ADD_STUDENT_TO_CLASS(state, { data, classIndex }) {
      state.monthlyClasses[classIndex].students.push(data);
    },
    DELETE_STUDENT_FROM_CLASS(state, { key, classIndex }) {
      let students = [...state.monthlyClasses[classIndex].students];
      let foundIndex = students.findIndex(
        student => student.studentKey === key
      );
      students.splice(foundIndex, 1);
      state.monthlyClasses[classIndex].students = students;
    },
    ADD_TRIAL_TO_CLASS(state, { data, classIndex }) {
      state.monthlyClasses[classIndex].trials.push(data);
    },
    ADD_MONTHLY(state, payload) {
      state.monthlyClasses = payload;
    },
    REMOVE_TRIAL(state, { classIndex, trialKey }) {
      let trials = [...state.monthlyClasses[classIndex].trials];
      let foundIndex = trials.findIndex(trial => trial.trialKey === trialKey);
      trials.splice(foundIndex, 1);
      state.monthlyClasses[classIndex].trials = trials;
    }
  },

  actions: {
    loadStudentsClass({ commit }, classKey) {
      return new Promise((resolve, reject) => {
        fs.collection("studentsClass")
          .where("classKey", "==", classKey)
          .where("active", "==", true)
          .get()
          .then(querySnapshot => {
            let val = [];
            querySnapshot.forEach(doc => {
              val.push({
                key: doc.id,
                ...doc.data()
              });
            });
            resolve(val);
          });
      });
    },
    loadTrialsClass({ commit }, classKey) {
      return new Promise((resolve, reject) => {
        fs.collection("trialsClass")
          .where("classKey", "==", classKey)
          .where("active", "==", true)
          .get()
          .then(querySnapshot => {
            let val = [];
            querySnapshot.forEach(doc => {
              val.push({
                key: doc.id,
                ...doc.data()
              });
            });
            resolve(val);
          });
      });
    },
    addStudentToClass({ commit }, { data, classIndex }) {
      return new Promise(async (resolve, reject) => {
        fs.collection("studentsClass")
          .add(data)
          .then(docRef => {
            commit("ADD_STUDENT_TO_CLASS", {
              data: { ...data, key: docRef.id },
              classIndex
            });
            resolve({ ...data, key: docRef.id });
          });
      });
    },
    addTrialToClass({ commit }, { data, classIndex }) {
      return new Promise(async (resolve, reject) => {
        fs.collection("trialsClass")
          .add(data)
          .then(docRef => {
            commit("ADD_TRIAL_TO_CLASS", {
              data: { ...data, key: docRef.id },
              classIndex
            });
            resolve({ ...data, key: docRef.id });
          });
      });
    },
    removeStudentFromClass({ commit }, { key, data, classIndex }) {
      return new Promise(async (resolve, reject) => {
        fs.collection("studentsClass")
          .doc(key)
          .update({ ...data })
          .then(() => {
            commit("DELETE_STUDENT_FROM_CLASS", {
              key,
              classIndex
            });
            resolve();
          })
          .catch(error => {
            console.error(error);
          });
      });
    },

    deleteStudentFromClass({ commit }, { key, classIndex }) {
      return new Promise(async (resolve, reject) => {
        fs.collection("studentsClass")
          .doc(key)
          .delete()
          .then(() => {
            commit("DELETE_STUDENT_FROM_CLASS", {
              key,
              classIndex
            });
            resolve();
          })
          .catch(error => {
            console.error(error);
          });
        // await db.ref(`classgroup/${classKey}/${key}`).remove();
        // resolve();
      });
    },
    deleteTrialFromClass({ commit }, { key, classIndex, trialKey }) {
      return new Promise(async (resolve, reject) => {
        fs.collection("trialsClass")
          .doc(key)
          .delete()
          .then(() => {
            commit("REMOVE_TRIAL", {
              classIndex,
              trialKey
            });
            resolve();
          })
          .catch(error => {
            console.error(error);
          });
      });
    }
  }
};
