export const formConfig = [
  {
    label: "User Name",
    fieldName: "name",
    type: "Input"
  },
  {
    label: "Email",
    fieldName: "email",
    type: "Input"
  },
  {
    label: "Password",
    fieldName: "password",
    type: "Input",
    required:false
  },
  {
    label: "Assigned Locations",
    fieldName: "assignedCentres",
    type: "Select",
    multiple: true,
    listName: "locations",
    value: "key",
    title: "name"
  },
  {
    label: "Roles",
    fieldName: "assignedRoles",
    type: "Select",
    listName: null,
    values: ["Administrator", "Head Coach", "Coach", "Manager", "Vendor"]
  }
];
