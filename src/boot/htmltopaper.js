import Vue from "vue";
import VueHtmlToPaper from "vue-html-to-paper";

const options = {
  name: "_blank",
  specs: ["fullscreen=yes", "titlebar=yes", "scrollbars=yes"],
  styles: [
    "https://cdn.jsdelivr.net/npm/quasar@1.9.14/dist/quasar.min.css",
    "https://fonts.googleapis.com/icon?family=Material+Icons"
  ]
};

Vue.use(VueHtmlToPaper, options);
