import Firebase from "firebase/app";
import "firebase/firestore";
import "firebase/database";
import "firebase/auth";

import config from "./.env.json";

if (!Firebase.apps.length) Firebase.initializeApp(config);
export default ({ Vue }) => {
  // Initialize Firebase from settings

  Vue.prototype.$firebase = Firebase;
};

export const db = Firebase.database();
export const fs = Firebase.firestore();
export const auth = Firebase.auth();
export const firebase = Firebase;
