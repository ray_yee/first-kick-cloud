import { fs } from "../boot/firebase";
import orderBy from "lodash/orderBy";

export default {
  namespaced: true,

  state: {
    attendance: null,
    trialAttendance: null,
    coachAttendance: null,
    coachMonthAttendance: null,
    adminAttendance: null
  },
  getters: {
    attendance(state) {
      return state.attendance;
    },
    coachAttendance(state) {
      return state.coachAttendance;
    },
    adminAttendance(state) {
      return state.adminAttendance;
    }
  },

  mutations: {
    SET_ATTENDANCE(state, payload) {
      state.attendance = payload;
    },
    SET_TRIAL_ATTENDANCE(state, payload) {
      state.trialAttendance = payload;
    },
    SET_COACH_ATTENDANCE(state, payload) {
      state.coachAttendance = payload;
    },
    SET_COACH_MONTH_ATTENDANCE(state, payload) {
      state.coachMonthAttendance = payload;
    },
    SET_ADMIN_ATTENDANCE(state, payload) {
      state.adminAttendance = payload;
    },
    ADD_ATTENDANCE(state, { data, newRef }) {
      state.attendance = {
        ...state.attendance,
        [data.studentKey]: {
          ...data,
          key: newRef.id
        }
      };
    },
    ADD_TRIAL_ATTENDANCE(state, { data, newRef }) {
      state.trialAttendance = {
        ...state.trialAttendance,
        [data.trialKey]: {
          ...data,
          key: newRef.id
        }
      };
    },
    ADD_COACH_ATTENDANCE(state, { classKey, data }) {
      let coachAttendance = { ...state.coachAttendance };
      if (coachAttendance[classKey] === undefined) {
        coachAttendance[classKey] = [];
      }
      coachAttendance[classKey].push(data);
      state.coachAttendance = coachAttendance;
    },
    ADD_ADMIN_ATTENDANCE(state, data) {
      state.adminAttendance.push(data);
    },
    REMOVE_ATTENDANCE(state, studentKey) {
      let newAttendance = { ...state.attendance };
      delete newAttendance[studentKey];
      state.attendance = newAttendance;
    },
    REMOVE_TRIAL_ATTENDANCE(state, trialKey) {
      let newAttendance = { ...state.trialAttendance };
      delete newAttendance[trialKey];
      state.trialAttendance = newAttendance;
    },
    REMOVE_COACH_ATTENDANCE(state, { classKey, index }) {
      let coachAttendance = { ...state.coachAttendance };
      coachAttendance[classKey].splice(index, 1);
      state.coachAttendance = coachAttendance;
    },
    REMOVE_ADMIN_ATTENDANCE(state, index) {
      state.adminAttendance.splice(index, 1);
    }
  },

  actions: {
    loadAttendance({ commit }, { date, locationId }) {
      return new Promise((resolve, reject) => {
        fs.collection("attendance")
          .where("locationId", "==", locationId)
          .where("date", "==", date)
          .get()
          .then(function(querySnapshot) {
            let attendance = {};
            querySnapshot.forEach(function(doc) {
              const { studentKey } = doc.data();
              attendance[studentKey] = {
                ...doc.data(),
                key: doc.id
              };
            });
            commit("SET_ATTENDANCE", attendance);
            resolve(attendance);
          })
          .catch(function(error) {
            reject();
            console.log("Error getting documents: ", error);
          });
      });
    },
    loadTrialAttendance({ commit }, { date, locationId }) {
      return new Promise((resolve, reject) => {
        fs.collection("trialAttendance")
          .where("locationId", "==", locationId)
          .where("date", "==", date)
          .get()
          .then(function(querySnapshot) {
            let attendance = {};
            querySnapshot.forEach(function(doc) {
              const { trialKey } = doc.data();
              attendance[trialKey] = {
                ...doc.data(),
                key: doc.id
              };
            });
            commit("SET_TRIAL_ATTENDANCE", attendance);
            resolve(attendance);
          })
          .catch(function(error) {
            reject();
            console.log("Error getting documents: ", error);
          });
      });
    },
    addAttendance({ commit }, data) {
      return new Promise(async resolve => {
        const newRef = await fs.collection("attendance").add(data);
        commit("ADD_ATTENDANCE", { data, newRef });
        resolve(newRef);
      });
    },
    removeAttendance({ commit }, { key, studentKey }) {
      return new Promise(async resolve => {
        await fs
          .collection("attendance")
          .doc(key)
          .delete();
        commit("REMOVE_ATTENDANCE", studentKey);
        resolve();
      });
    },
    addTrialAttendance({ commit }, data) {
      return new Promise(async resolve => {
        const newRef = await fs.collection("trialAttendance").add(data);
        commit("ADD_TRIAL_ATTENDANCE", { data, newRef });
        resolve(newRef);
      });
    },
    removeTrialAttendance({ commit }, { key, trialKey }) {
      return new Promise(async resolve => {
        await fs
          .collection("trialAttendance")
          .doc(key)
          .delete();
        commit("REMOVE_TRIAL_ATTENDANCE", trialKey);
        resolve();
      });
    },

    loadCoachAttendance({ commit }, { date, locationId }) {
      return new Promise((resolve, reject) => {
        fs.collection("coachAttendances")
          .where("locationId", "==", locationId)
          .where("date", "==", date)
          .get()
          .then(function(querySnapshot) {
            let todayAttendance = {};
            querySnapshot.forEach(function(doc) {
              const coach = doc.data();
              if (todayAttendance[coach.class] === undefined) {
                todayAttendance[coach.class] = [];
              }
              todayAttendance[coach.class].push({
                ...doc.data(),
                key: doc.id
              });
            });
            commit("SET_COACH_ATTENDANCE", todayAttendance);
            resolve(todayAttendance);
          })
          .catch(function(error) {
            reject();
            console.log("Error getting documents: ", error);
          });
      });
    },
    loadCoachAttendanceByDates({ commit }, { dates, locationId }) {
      return new Promise((resolve, reject) => {
        fs.collection("coachAttendances")
          .where("locationId", "==", locationId)
          .where("date", "in", dates)
          .get()
          .then(function(querySnapshot) {
            let coachAttendance = [];
            querySnapshot.forEach(function(doc) {
              const coach = doc.data();
              coachAttendance.push({
                ...coach,
                key: doc.id
              });
            });
            coachAttendance = orderBy(coachAttendance, ["date"]);
            let groupedCoachAttendance = {};
            coachAttendance.map(att => {
              if (groupedCoachAttendance[att.coachKey] === undefined) {
                groupedCoachAttendance[att.coachKey] = {};
              }
              if (
                groupedCoachAttendance[att.coachKey][att.date] === undefined
              ) {
                groupedCoachAttendance[att.coachKey][att.date] = [];
              }
              groupedCoachAttendance[att.coachKey][att.date].push(att);
            });

            if (Object.keys(groupedCoachAttendance).length > 0) {
              commit("SET_COACH_MONTH_ATTENDANCE", groupedCoachAttendance);
            } else {
              commit("SET_COACH_MONTH_ATTENDANCE", null);
            }
            resolve(coachAttendance);
          })
          .catch(function(error) {
            reject();
            console.log("Error getting documents: ", error);
          });
      });
    },
    addCoachAttendance({ commit }, { data, classKey }) {
      return new Promise(async resolve => {
        const newRef = await fs.collection("coachAttendances").add(data);
        commit("ADD_COACH_ATTENDANCE", {
          classKey,
          data: {
            ...data,
            key: newRef.id
          }
        });
        resolve(newRef);
      });
    },

    removeCoachAttendance({ commit }, { key, classKey, index }) {
      return new Promise(async resolve => {
        await fs
          .collection("coachAttendances")
          .doc(key)
          .delete();
        commit("REMOVE_COACH_ATTENDANCE", { classKey, index });
        resolve();
      });
    },
    loadAdminAttendance({ commit }, { date, locationId }) {
      return new Promise((resolve, reject) => {
        fs.collection("adminAttendance")
          .where("locationId", "==", locationId)
          .where("date", "==", date)
          .get()
          .then(function(querySnapshot) {
            let todayAttendance = [];
            querySnapshot.forEach(function(doc) {
              todayAttendance.push({
                ...doc.data(),
                key: doc.id
              });
            });
            commit("SET_ADMIN_ATTENDANCE", todayAttendance);
            resolve(todayAttendance);
          })
          .catch(function(error) {
            reject();
            console.log("Error getting documents: ", error);
          });
      });
    },
    addAdminAttendance({ commit }, data) {
      return new Promise(async resolve => {
        const newRef = await fs.collection("adminAttendance").add(data);
        commit("ADD_ADMIN_ATTENDANCE", {
          ...data,
          key: newRef.id
        });
        resolve(newRef);
      });
    },

    removeAdminAttendance({ commit }, { key, index }) {
      return new Promise(async resolve => {
        await fs
          .collection("adminAttendance")
          .doc(key)
          .delete();
        commit("REMOVE_ADMIN_ATTENDANCE", index);
        resolve();
      });
    }

    // deleteAllAttendance({ commit, state }, key) {
    //   return new Promise(async resolve => {
    //     fs.collection("attendance")
    //       .where("studentKey", "==", key)
    //       .get()
    //       .then(function(querySnapshot) {
    //         let attendance = [];
    //         querySnapshot.forEach(function(doc) {
    //           fs.collection("attendance")
    //             .doc(doc.id)
    //             .delete();
    //         });
    //         resolve();
    //       });
    //     // await fs
    //     //   .collection("attendance")
    //     //   .doc(key)
    //     //   .delete();
    //   });
    // }
  }
};
