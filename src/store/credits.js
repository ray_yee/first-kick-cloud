import { fs } from "../boot/firebase";
import moment from "moment";

export default {
  namespaced: true,

  state: {
    list: null,
    listByKey: null,
    used: null,
    unused: null,
    usedByKey: null
  },

  mutations: {
    SET_CREDITS(state, payload) {
      state.listByKey= payload;
      let list = []
      Object.keys(payload).forEach(key=> {
       list.push({
         ...payload[key],
         key
       })
      
      })
      state.list= list
    },
    SET_USED_CREDITS(state, payload) {
      state.usedByKey= payload;
    
    },
    SET_ALL_CREDITS(state, payload) {
      let used = [], unused = []
      payload.forEach(credit=> {
        if (credit.used) {
          used.push(credit)
        }
        else {
          unused.push(credit)
        }
      })
      state.used = used
      state.unused = unused
    }
  },

  actions: {
    loadAllCredits({ commit }) {
      return new Promise((resolve, reject) => {
        fs.collection("credits")
        .onSnapshot((querySnapshot)=> {
            let credits = [];
            querySnapshot.forEach(function(doc) {
              credits.push({
                ...doc.data(),
                key: doc.id
              });
            });
            commit("SET_ALL_CREDITS", credits);
            resolve(credits);
          })
    
      });
    },
    loadUsedCredits({ commit }) {
      return new Promise((resolve, reject) => {
        fs.collection("credits")
        .where("used", "==", true)
        .onSnapshot((querySnapshot)=> {
          let credits = {};
          querySnapshot.forEach(function(doc) {
            credits[doc.id] = {
              ...doc.data()
            };
          });
        
            commit("SET_USED_CREDITS", credits);
            resolve(credits);
          })
    
      });
    },
    loadCredits({ commit }) {
      return new Promise((resolve, reject) => {
        fs.collection("credits")
        .where("used", "==", false)
        .onSnapshot((querySnapshot)=> {
          let credits = {};
          querySnapshot.forEach(function(doc) {
            credits[doc.id] = {
              ...doc.data()
            };
          });
        
            commit("SET_CREDITS", credits);
            resolve(credits);
          })
    
      });
    },
    updateCredit({ commit }, {key, data}) {
      return fs.collection("credits").doc(key).update(data)
    },
    deleteCredit({ commit }, key) {
      return fs.collection("credits").doc(key).delete();
    },
    addCredit({commit}, data) {
      return new Promise(async (resolve, reject) => {
        fs.collection("credits").add(data).then((docRef) => {
          resolve(docRef.id);
      })
        
      });
    },
    addBulkCredits({commit}, students) {
      return new Promise(async (resolve, reject)=> {
        // Get a new write batch
        let batch = fs.batch();
        let promises = []
        students.forEach((student) => {
          let a = new Promise(async (res, rej)=> {
          let newRef = fs.collection("credits").doc();
          const {creditAmount} = student
          batch.set(newRef, {
            dateIssued: moment().format(),
            used: false,
            amount: parseFloat(creditAmount),
            studentKey: student.key
          });
          promises.push(a)
          res()
        })
        })
        await Promise.all(promises)
        batch.commit().then(() => {
          resolve()
      });
      })
    }
  }
};
