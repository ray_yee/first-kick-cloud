import { fs } from "../boot/firebase";

export default {
  namespaced: true,

  state: {
    locations: null,
    pages: null,
    blogs: null,
    news: null,
    faqs: null
  },

  mutations: {
    SET_LOCATIONS(state, payload) {
      state.locations = payload;
    },
    ADD_LOCATION(state, { locationKey, data }) {
      state.locations = {
        ...state.locations,
        [locationKey]: data
      };
    },
    SET_PAGES(state, payload) {
      state.pages = payload;
    },
    ADD_PAGE(state, { key, data }) {
      state.pages = {
        ...state.pages,
        [key]: { ...data }
      };
    },
    SET_BLOGS(state, payload) {
      state.blogs = payload;
    },
    ADD_BLOG(state, { key, data }) {
      state.blogs = {
        ...state.blogs,
        [key]: { ...data }
      };
    },
    SET_NEWS(state, payload) {
      state.news = payload;
    },
    ADD_NEWS(state, { key, data }) {
      state.news = {
        ...state.news,
        [key]: { ...data }
      };
    },
    UPDATE_NEWS(state, { key, data }) {
      state.news = {
        ...state.news,
        [key]: { ...data }
      };
    },
    SET_FAQS(state, payload) {
      state.faqs = payload;
    },
  },

  actions: {
    loadLocations({ commit }) {
      return fs
          .collection("cms")
          .doc("content")
          .collection("locations")
          .onSnapshot((snapshot) => {
            let val = {};
            snapshot.forEach(doc => {
              val[doc.id] = { key: doc.id, ...doc.data() };
            });
            commit("SET_LOCATIONS", val);
          })

    },
    addLocation({ commit }, { locationKey, data }) {
      return new Promise(async (resolve, reject) => {
        const res = await fs
          .collection("cms")
          .doc("content")
          .collection("locations")
          .doc(locationKey)
          .set(data);
        commit("ADD_LOCATION", { locationKey, data });
        resolve();
      });
    },
    updateLocation({commit},{locationKey, data}) {
      console.log(locationKey, data)
      return fs
          .collection("cms")
          .doc("content")
          .collection("locations").doc(locationKey)
          .update(data);
 
    },
    loadPages({ commit }) {
      return new Promise(async (resolve, reject) => {
        let snapshot = await fs
          .collection("cms")
          .doc("content")
          .collection("pages")
          .get();

        if (snapshot.empty) {
          console.log("No such document!");
          resolve();
        } else {
          let val = {};
          snapshot.forEach(doc => {
            val[doc.id] = { key: doc.id, ...doc.data() };
          });
          commit("SET_PAGES", val);
          resolve();
        }
      });
    },

    addPage({ commit }, data) {
      return new Promise(async (resolve, reject) => {
        const res = await fs
          .collection("cms")
          .doc("content")
          .collection("pages")
          .add(data);
        commit("ADD_PAGE", { key: res.id, data });
        resolve();
      });
    },
    loadBlogs({ commit }) {
      return new Promise(async (resolve, reject) => {
        fs
          .collection("blogs")
          .onSnapshot((snapshot) => {
            let val = {};
          snapshot.forEach(doc => {
            val[doc.id] = { key: doc.id, ...doc.data() };
          });
            commit("SET_BLOGS", val);
            resolve();
        });

      });
    },
    bulkCopyNews({commit}, data) {
      return new Promise(async (resolve, reject) => {
        let batch = fs.batch();
        Object.keys(data).forEach(key => {
          let newRef = fs.collection('news').doc(key)
          batch.set(newRef, {...data[key]})
        })
        batch.commit().then(() => {
   resolve()
 })       
      });
    },

    addBlog({ commit }, data) {
      return fs
          .collection("blogs")
          .add(data);
    
    },
    updateBlog({commit},{key, data}) {
      return fs
          .collection("blogs").doc(key)
          .update(data);
 
    },
    deleteBlog({commit}, key) {
      return fs
          .collection("blogs").doc(key).delete()
    },
    loadNews({ commit }) {
      return new Promise(async (resolve, reject) => {
        fs
          .collection("news")
          .onSnapshot((snapshot) => {
            if (snapshot.empty) {
              console.log("No such document!");
              resolve();
            } else {
              let val = {};
              snapshot.forEach(doc => {
                val[doc.id] = { key: doc.id, ...doc.data() };
              });
              commit("SET_NEWS", val);
              resolve();
            }
        });

      });
     
    },

    addNews({ commit }, data) {
      return fs
          .collection("news")
          .add(data);
      
    },
    updateNews({ commit }, { key, data }) {
      return fs
          .collection("news")
          .doc(key)
          .update(data);

    },
    deleteNews({commit}, key) {
      return fs
      
          .collection("news").doc(key).delete()
    },
    loadFaqs({ commit }) {
      return new Promise(async (resolve, reject) => {
        fs
          .collection("faqs")
          .onSnapshot((snapshot) => {
            let val = {};
          snapshot.forEach(doc => {
            val[doc.id] = { key: doc.id, ...doc.data() };
          });
            commit("SET_FAQS", val);
            resolve();
        });

      });
    },
    addFaqs({ commit }, data) {
      return fs
          .collection("faqs")
          .add(data);
    
    },
    updateFaqs({commit},{key, data}) {
      return fs
          .collection("faqs").doc(key)
          .update(data);
 
    },
    deleteFaqs({commit}, key) {
      return fs
          .collection("faqs").doc(key).delete()
    },
  }
};
