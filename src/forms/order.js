export const formConfig = [
    {
        label: "Select Student",
        fieldName: "studentKey",
        type: "Select",
        listName: "students",
        value: "key",
        title: "childName"
      },
    {
      label: "Jersey Size",
      fieldName: "jerseySize",
      type: "Select",
      values: [
        'U6 (113-122cm)',
        'U8 (131-140cm)',
        'U10 (141-150cm)',
        'U12 (151-160cm)',
        '14-15 (161-172cm)',
        'Adult S',
        'Adult M',
        'Adult L'
      ],
      listName: null
    },{
        label: "Short Size",
        fieldName: "shortSize",
        type: "Select",
        values: [
          'U6 (113-122cm)',
          'U8 (131-140cm)',
          'U10 (141-150cm)',
          'U12 (151-160cm)'
        ],
        listName: null
      },
      {
        label: "Print Text",
        fieldName: "printText",
        type: "Input"
      },
      {
        label: "Paid",
        fieldName: "paymentStatus",
        type: "Toggle",
        options: ['Not Paid', 'Paid']
      },
      {
        label: "Date Payment Received",
        fieldName: "paymentReceived",
        type: "DatePicker",
        default: 'Today'
      },
      {
        label: "Registration",
        fieldName: "registrationKit",
        type: "Toggle"
      },
      {
        label: "Sibling Discount",
        fieldName: "siblingDiscount",
        type: "Toggle"
      },
  ];
  