import {
  db,
  fs,
  firebase
} from "../boot/firebase";
import generator from "generate-password";
import {
  nodeServer
} from "../configs/site";

export default {
  namespaced: true,

  state: {
    list: [],
    listByKey: null,
    users: []
  },

  mutations: {
    SET_ACCOUNTS(state, payload) {
      state.listByKey = payload;
      let accounts = [];
      Object.keys(payload).forEach(key => {
        accounts.push({
          ...payload[key],
          key
        });
      });
      state.list = accounts;
    },
    SET_ACCOUNT_USERS(state, payload) {
      state.users = payload
    }
  },

  actions: {
    loadAllUsers({
      commit
    }) {
      return new Promise(async (resolve, reject) => {
        this._vm
          .$axios({
            method: "get",
            url: `${nodeServer}/accounts/all`,
          }).then(resp => {
            if (resp.status === 200) {
              commit('SET_ACCOUNT_USERS', resp.data)
            }
            resolve()
          })
      })
    },
    loadAccounts({
      commit
    }) {
      return new Promise((resolve, reject) => {
        fs.collection("accounts")
          .onSnapshot(function (querySnapshot) {
            let accounts = {};
            querySnapshot.forEach(function (doc) {
              accounts[doc.id] = {
                ...doc.data()
              };
            });
            commit("SET_ACCOUNTS", accounts);
            resolve(accounts);
          })

      });
    },
    fetchAccount({commit}, key) {
      return new Promise((resolve, reject) => {
        fs.collection("accounts").doc(key)
          .get().then(doc => {
            if(doc.exists) {
              resolve({...doc.data(), key: doc.id})
            }
            });
          })
    },
    updateRegistrationPaid({ commit }, {studentKey, paymentKey, key}) {
      return fs
          .collection("accounts")
          .doc(key)
          .update({
            registrationPaid: firebase.firestore.FieldValue.arrayUnion(studentKey),
            registrationPaymentKey: firebase.firestore.FieldValue.arrayUnion(paymentKey),
          });
      
    },
    updateAccount({
      commit
    }, {
      key,
      data
    }) {
      return new Promise(async (resolve, reject) => {
        await fs
          .collection("accounts")
          .doc(key)
          .update({
            children: firebase.firestore.FieldValue.arrayUnion(data.child)
          });
        resolve();
      });
    },
    createUserAccount({
      commit
    }, {
      email,
      password
    }) {
      return new Promise(async (resolve, reject) => {
        this._vm
          .$axios({
            method: "post",
            url: `${nodeServer}/accounts/create`,
            data: {
              email,
              password
            }
          }).then(resp => {
            if (resp.data.code === 'auth/email-already-exists') {
              resolve('Email exist')
            } else {
              resolve(resp.data)

            }
          })
      })
    },
    changeAccountEmail({
      commit
    }, {
      account,
      email
    }) {
      return new Promise(async (resolve, reject) => {
        this._vm
          .$axios({
            method: "post",
            url: `${nodeServer}/accounts/get`,
            data: {
              email
            }
          })
          .then(resp => {
            if (!resp.data) {
              const password = generator.generate({
                length: 10,
                numbers: true,
                uppercase: false
              });
              this._vm
                .$axios({
                  method: "post",
                  url: `${nodeServer}/accounts/create`,
                  data: {
                    email,
                    password
                  }
                })
                .then(resp => {
                  account.children.forEach(async (childKey) => {
                    await db
                      .ref("/students/" + childKey)
                      .update({
                        account: resp.data
                      });
                  })
                  fs.collection('paymentAdvices').where("email", "==", account.email).get()
                    .then((querySnapshot) => {
                      if (querySnapshot.size > 0) {
                        let batch = fs.batch();
                        querySnapshot.forEach((doc) => {
                          // doc.data() is never undefined for query doc snapshots
                          let docRef = fs.collection("paymentAdvices").doc(doc.id)
                          batch.update(docRef, {
                            email
                          })
                        });
                        batch.commit()
                      }
                    })

                  fs.collection("accounts")
                    .doc(resp.data)
                    .set({
                      ...account,
                      key: resp.data,
                      email
                    })
                    .then(async () => {
                      await fs.collection("accounts")
                      .doc(account.key).delete()
                      this._vm
                        .$axios({
                          method: "post",
                          url: `${nodeServer}/accounts/delete`,
                          data: {
                            uid: account.key
                          }
                        })
                      resolve({
                        password,
                        account: resp.data
                      });
                    })
                    .catch(function (error) {
                      console.log(data);
                      console.error("Error writing document: ", error);
                      reject(error);
                    });
                }).catch(err => {
                  console.log(err)
                });
            } else {
              reject('Email is already in use!')
            }
          });
      });
    },
    deleteUserAccount({
      commit
    }, email) {
      return new Promise(async (resolve, reject) => {
        this._vm
          .$axios({
            method: "post",
            url: `${nodeServer}/accounts/get`,
            data: {
              email
            }
          })
          .then(resp => {
            if (!resp.data) {
              resolve()
            } else {
              const {
                uid
              } = resp.data
              this._vm
                .$axios({
                  method: "post",
                  url: `${nodeServer}/accounts/delete`,
                  data: {
                    uid
                  }
                })
                .then(async resp => {
                  console.log(resp)
                  resolve()
                })
            }
          })
      })
    },

    createAccount({
      commit
    }, {
      email,
      studentKey,
      data
    }) {
      return new Promise(async (resolve, reject) => {
        this._vm
          .$axios({
            method: "post",
            url: `${nodeServer}/accounts/get`,
            data: {
              email
            }
          })
          .then(resp => {
            if (!resp.data) {
              const password = generator.generate({
                length: 10,
                numbers: true,
                uppercase: false
              });
              this._vm
                .$axios({
                  method: "post",
                  url: `${nodeServer}/accounts/create`,
                  data: {
                    email,
                    password
                  }
                })
                .then(async resp => {
                  console.log(resp)
                  await db
                    .ref("/students/" + studentKey)
                    .update({
                      account: resp.data
                    });
                  console.log(data)
                  fs.collection("accounts")
                    .doc(resp.data)
                    .set(data)
                    .then(() => {
                      console.log("Document successfully written!");
                      resolve({
                        password,
                        account: resp.data
                      });
                    })
                    .catch(function (error) {
                      console.log(data);
                      console.error("Error writing document: ", error);
                      reject(error);
                    });
                }).catch(err => {
                  console.log(err)
                });
            } else {
              fs.collection("accounts")
                .doc(resp.data.uid)
                .set(data)
                .then(() => {
                  console.log("Document successfully written!");
                  resolve({
                    account: {
                      key: resp.data.uid
                    }
                  })
                })
                .catch(function (error) {
                  console.log(data);
                  console.error("Error writing document: ", error);
                  reject(error);
                });

            }
          });
      });
    }
  }
};
