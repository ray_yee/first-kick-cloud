import { db } from "../boot/firebase";
import orderBy from "lodash/orderBy";
export default {
  namespaced: true,

  state: {
    list: [],
    listByKey: null,
    listByDate : []
  },

  mutations: {
    SET_LEADS(state, payload) {
      let leads = [];
      Object.keys(payload).forEach(key => {
        leads.push({
          ...payload[key],
          key
        });
      });
      state.listByKey = payload;
      state.list = orderBy(leads, "dateAdded", "desc");
    },
    SET_LEADS_BY_DATE(state, payload) {
      state.listByDate = payload
    }
  },

  actions: {
    loadAllLeads({ commit }) {
      return new Promise((resolve, reject) => {
        db.ref("leads").on("value", snapshot => {
          let val = snapshot.val();
          if (val) {
            commit("SET_LEADS", val);
            resolve(val);
          }
          resolve();
        });
      });
    },
    loadLeads({ commit }, location) {
      return new Promise((resolve, reject) => {
        db.ref("leads")
          .orderByChild("location")
          .equalTo(location)
          .on("value", snapshot => {
            let val = snapshot.val();
            if (val) {
              commit("SET_LEADS", val);
              resolve(val);
            }
            resolve();
          });
      });
    },
    loadLeadsByDate({commit}, {date, location}) {
      return new Promise((resolve, reject) => {
        db.ref("leads")
          .orderByChild("dateOfTrial")
          .equalTo(date)
          .on("value", snapshot => {
            let val = snapshot.val();
            if (val) {
              let leads = [];
              Object.keys(val).forEach(key => {
                if (val[key].location === location)
                leads.push({
                  ...val[key],
                  key
                });
              });
              commit("SET_LEADS_BY_DATE", leads);
              resolve(leads);
            }
            resolve();
          });
      });
    },


    addLead({ commit }, leadData) {
      return new Promise(async (resolve, reject) => {
        const newRef = await db.ref("leads").push(leadData);
        // commit("ADD_LEAD", { ...leadData, key: newRef.key });
        resolve();
      });
    },
    updateLead({ commit }, { key, data }) {
      return new Promise(async (resolve, reject) => {
        await db
          .ref("leads")
          .child(key)
          .update(data);

        resolve();
      });
    },
    addLeadActivity({ commit }, { key, data }) {
      return new Promise(async (resolve, reject) => {
        await db.ref("leads/" + key + "/activities").push(data);
        resolve();
      });
    },
    deleteLead({ commit }, key) {
      return new Promise(async (resolve, reject) => {
        await db
          .ref("leads")
          .child(key)
          .remove();

        resolve();
      });
    }
  }
};
