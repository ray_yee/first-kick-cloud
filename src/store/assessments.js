import { fs } from "../boot/firebase";

export default {
  namespaced: true,

  state: {
    list: null
  },

  mutations: {
    SET_ASSESSMENTS(state, payload) {
      state.list = payload;
    }
  },

  actions: {
    loadAssessments({ commit }, { date, location }) {
      return new Promise((resolve, reject) => {
        fs.collection("assessments")
          .where("location", "==", location)
          .where("date", "==", date)
          .onSnapshot(function(querySnapshot) {
            let assessments = {};
            querySnapshot.forEach(function(doc) {
              const { studentKey } = doc.data();
              assessments[studentKey] = {
                ...doc.data(),
                key: doc.id
              };
            });
            commit("SET_ASSESSMENTS", assessments);
            resolve(assessments);
          });
      });
    },
    addAssessment({ commit }, data) {
      return fs.collection("assessments").add(data);
    },
    updateAssessment({ commit }, { key, data }) {
      return fs
        .collection("assessments")
        .doc(key)
        .update(data);
    },
    removeAssessment({ commit }, key) {
      return fs
        .collection("assessments")
        .doc(key)
        .delete();
    }
  }
};
