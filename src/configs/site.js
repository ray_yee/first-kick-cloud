export const nodeServer = "https://fkanode.el.r.appspot.com";
//export const nodeServer = "http://localhost:8020";

export const links = [
  {
    title: "Home",
    icon: "home",
    link: "/",
    roles: ["Coach", "Head Coach", "Manager", "Superadmin", "Vendor"]
  },
  {
    title: "Leads",
    icon: "group_work",
    link: "/location/leads",
    roles: [ "Manager", "Superadmin"]
  },
  {
    title: "Orders",
    icon: "shopping_cart",
    link: "/orders",
    roles: ["Manager", "Superadmin", "Vendor"]
  },
  {
    link: "/students",
    icon: "group",
    title: "Students",
    roles: ["Manager", "Superadmin"]
  },
  {
    link: "/students/details",
    icon: "assignment_ind",
    title: "Student Details",
    roles: ["Manager", "Superadmin"]
  },
  {
    link: "/location/payments",
    icon: "local_atm",
    title: "Payments",
    roles: ["Manager", "Superadmin"]
  },
  
  {
    link: "/location/paymentadvices",
    icon: "receipt",
    title: "Payment Advices",
    roles: ["Manager", "Superadmin"]
  },
  {
    link: "/paynow",
    icon: "qr_code",
    title: "PayNow",
    roles: ["Manager", "Superadmin"]
  },
  {
    link: "/credits",
    icon: "account_balance_wallet",
    title: "Credits",
    roles: ["Manager", "Superadmin"]
  },
  {
    title: "Attendance",
    icon: "done_outline",
    link: "/location/attendance",
    roles: ["Head Coach", "Manager", "Superadmin"]
  },
  {
    title: "Attendance Summary",
    icon: "view_module",
    link: "/location/attendance/summary",
    roles: ["Manager", "Superadmin"]
  },

  // {
  //   link: "/upload-students",
  //   icon: "group",
  //   title: "Upload Students",
  //   roles: ["Manager", "Superadmin"]
  // },
  {
    title: "Classes",
    icon: "important_devices",
    roles: ["Manager", "Superadmin"],
    link: "/classes"
  },
  {
    title: "Accounts",
    icon: "account_box",
    roles: ["Manager", "Superadmin"],
    link: "/accounts"
  },
  {
    title: "Coaches",
    icon: "manage_accounts",
    link: "/coaches",
    roles: ["Manager", "Superadmin"]
  },
  {
    title: "Coaching Course",
    icon: "sports_soccer",
    link: "/coachingcourse",
    roles: ["Manager", "Superadmin"]
  },
  // {
  //   title: "Trials",
  //   icon: "group_work",
  //   link: "/trials",
  //   roles: ["Head Coach", "Manager", "Superadmin"]
  // },

  // {
  //   title: "Copy Credits",
  //   icon: "local_atm",
  //   link: "/copycredits",
  //   roles: ["Manager", "Superadmin"]
  // },
  // {
  //   title: "Copy Classes",
  //   icon: "local_atm",
  //   link: "/copyclasses",
  //   roles: ["Manager", "Superadmin"]
  // },
  // {
  //   title: "Monthly Classes",
  //   icon: "date_range",
  //   roles: ["Manager", "Superadmin"],
  //   link: "/location/monthly-classes"
  // },

  // {
  //   link: "/location/archived",
  //   icon: "archive",
  //   title: "Archived",
  //   roles: ["Manager", "Superadmin"]
  // },
  {
    title: "Training",
    icon: "trending_up",
    type: "parent",
    roles: ["Manager", "Superadmin"],
    children: [
      {
        link: "/training-plans",
        icon: "summarize",
        title: "Training Plans",
        roles: ["Manager", "Superadmin"]
      }
    ]
  },
  {
    title: "Reports",
    icon: "assessment",
    type: "parent",
    roles: ["Manager", "Superadmin"],
    children: [
      {
        link: "/location/reports/leads-conversion",
        icon: "group_work",
        title: "Leads Conversion",
        roles: ["Manager", "Superadmin"]
      },
      {
        link: "/location/reports/coaches-pay",
        icon: "list",
        title: "Coaches Pay",
        roles: ["Manager", "Superadmin"]
      }
    ]
  },
  {
    title: "Content",
    icon: "article",
    type: "parent",
    roles: ["Manager", "Superadmin"],
    children: [
      {
        link: "/cms/blogs",
        icon: "view_quilt",
        title: "Blogs",
        roles: ["Manager", "Superadmin"]
      },
      {
        link: "/cms/news",
        icon: "wysiwyg",
        title: "News",
        roles: ["Manager", "Superadmin"]
      },
      {
        link: "/cms/pages",
        icon: "view_quilt",
        title: "Pages",
        roles: ["Manager", "Superadmin"]
      },
      {
        link: "/cms/faqs",
        icon: "info",
        title: "FAQs",
        roles: ["Manager", "Superadmin"]
      },
      {
        link: "/cms/locations",
        icon: "edit_location",
        title: "Locations",
        roles: ["Manager", "Superadmin"]
      }
    ]
  },
  {
    title: "Settings",
    icon: "settings",
    type: "parent",
    roles: ["Manager", "Superadmin"],
    children: [
      {
        link: "/users",
        icon: "account_box",
        title: "Users",
        roles: ["Manager", "Superadmin"]
      },
      {
        link: "/locations",
        icon: "edit_location",
        title: "Locations",
        roles: ["Manager", "Superadmin"]
      },
      {
        link: "/fees-settings",
        icon: "monetization_on",
        title: "Fees",
        roles: ["Manager", "Superadmin"]
      }
    ]
  }
];

export const months = [
  "January",
  "February",
  "March",
  "April",
  "May",
  "June",
  "July",
  "August",
  "September",
  "October",
  "November",
  "December"
];
