import { db } from "../boot/firebase";

export default {
  namespaced: true,

  state: {
    users: [],
    usersByKey: null
  },
  getters: {
    users(state) {
      return state.users;
    }
  },
  getters: {
    users(state) {
      return state.users;
    }
  },

  mutations: {
    SET_USERS(state, payload) {
      state.usersByKey = payload;
      let users = [];
      Object.keys(payload).forEach(key => {
        users.push({
          ...payload[key],
          key
        });
      });
      state.users = users;
    }
  },

  actions: {
    loadUsers({ commit }) {
      return new Promise((resolve, reject) => {
        db.ref("users").on("value", snapshot => {
          let val = snapshot.val();
          if (val) {
            commit("SET_USERS", val);
            resolve(val);
          }
          resolve();
        });
      });
    },
    addUser({ commit }, data) {
      return new Promise(async (resolve, reject) => {
        const newRef = await db.ref("users").push(data);
        resolve();
      });
    },
    updateUser({ commit }, { data, key }) {
      return new Promise(async (resolve, reject) => {
        await db.ref("users/" + key).update(data);
        resolve();
      });
    },
    deleteUser({ commit }, key) {
      return new Promise(async (resolve, reject) => {
        await db.ref("users").child(key).remove();
        resolve();
      });
    },
  }
};
