import { db } from "../boot/firebase";

export default {
  namespaced: true,

  state: {
    list: [],
    listByKey: null
  },

  mutations: {
    SET_FEES(state, payload) {
      state.listByKey = payload;
      let fees = [];
      Object.keys(payload).forEach(key => {
        fees.push({
          ...payload[key],
          key
        });
      });
      state.list = fees;
    }
  },

  actions: {
    loadFees({ commit }) {
      return new Promise((resolve, reject) => {
        db.ref("fees").on("value", snapshot => {
          let val = snapshot.val();
          if (val) {
            commit("SET_FEES", val);
            resolve(val);
          }
          resolve();
        });
      });
    },
    addFees({ commit }, data) {
      return new Promise(async (resolve, reject) => {
        const newRef = await db.ref("fees").push(data);
        resolve();
      });
    },
    updateFees({ commit }, { data, key }) {
      return new Promise(async (resolve, reject) => {
        await db.ref("fees/" + key).update(data);
        resolve();
      });
    }
  }
};
