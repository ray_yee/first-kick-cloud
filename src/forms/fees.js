export const formConfig = [
  {
    label: "Fee Name",
    fieldName: "name",
    type: "Input"
  },
  {
    label: "Type",
    fieldName: "type",
    type: "Select",
    listName: null,
    values: ["Term", "Monthly"]
  }
];
