import { db } from "../boot/firebase";
import orderBy from "lodash/orderBy";

export default {
  namespaced: true,

  state: {
    list: [],
    userLocations: null,
    selectedLocation: null
  },

  getters: {
    locations(state) {
      return state.list;
    },
    userLocations(state) {
      return state.userLocations;
    }
  },

  mutations: {
    SET_LOCATIONS(state, { locations, userLocations }) {
      state.list = locations;
      state.userLocations = userLocations;
      state.selectedLocation = userLocations.find(loc => loc.active);
    },
    UPDATE_SELECTED_LOCATION(state, location) {
      state.selectedLocation = location;
    },
    UPDATE_LOCATION_DETAILS(state, { key, data }) {
      state.list = state.list.map(l => {
        if (l.key === key) {
          return {
            ...l,
            ...data
          };
        }
        return l;
      });
    },
    ADD_LOCATION_DETAILS(state, { data, key }) {
      let newLocations = state.list;
      newLocations[key] = data;
      state.list = newLocations;
    },
    ADD_CLASS(state, { data, locationKey }) {
      state.list = state.list.map(loc => {
        if (loc.key === locationKey) {
          let newLocationData = {
            ...loc,
            classes: loc.classes
              ? {
                  ...loc.classes,
                  [data.key]: data
                }
              : {
                  [data.key]: data
                }
          };
          return newLocationData;
        } else return loc;
      });
    },
    UPDATE_CLASS(state, { data, locationKey }) {
      state.list = state.list.map(loc => {
        if (loc.key === locationKey) {
          let newLocationData = {
            ...loc,
            classes: loc.classes
              ? {
                  ...loc.classes,
                  [data.key]: data
                }
              : {
                  [data.key]: data
                }
          };
          return newLocationData;
        } else return loc;
      });
    },
    REMOVE_CLASS(state, { classKey, locationKey }) {
      state.list = state.list.map(loc => {
        if (loc.key === locationKey) {
          let newClasses = { ...loc.classes };
          delete newClasses[classKey];

          return {
            ...loc,
            classes: newClasses
          };
        } else return loc;
      });
    }
  },

  actions: {
    loadLocations({ commit }, assignedCentres) {
      return new Promise((resolve, reject) => {
        db.ref("locations")
          // .orderByChild("active")
          // .equalTo(true)
          .once("value", snapshot => {
            const fbLocations = snapshot.val();
            if (fbLocations) {
              let locations = [];
              let userLocations = [];
              Object.keys(fbLocations).forEach(key => {
                if (assignedCentres.includes(key))
                  userLocations.push({
                    ...fbLocations[key],
                    key
                  });
                locations.push({
                  ...fbLocations[key],
                  key
                });
              });
              locations = orderBy(locations, ["active", "name"], ["desc"]);
              commit("SET_LOCATIONS", { locations, userLocations });
              resolve();
            }
          })
          .catch(err => {
            reject(err);
            console.log("Error getting documents: ", err);
          });
      });
    },
    updateLocationDetails({ commit }, { key, data }) {
      return new Promise((resolve, reject) => {
        db.ref("locations")
          .child(key)
          .update(data)
          .then(res => {
            commit("UPDATE_LOCATION_DETAILS", { key, data });
            resolve();
          })
          .catch(err => {
            reject(err);
            console.log("Error getting documents: ", err);
          });
      });
    },
    addLocationDetails({ commit }, data) {
      return new Promise(async (resolve, reject) => {
        const newRef = await db.ref("locations").push(data);
        commit("ADD_LOCATION", { ...data, key: newRef.key });
        resolve();
      });
    },
    addClass({ commit }, { data, locationKey }) {
      return new Promise(async (resolve, reject) => {
        const newRef = await db
          .ref("locations/" + locationKey + "/classes")
          .push(data);
        commit("ADD_CLASS", {
          data: { ...data, key: newRef.key },
          locationKey
        });
        resolve(newRef.key);
      });
    },
    updateClass({ commit }, { data, classKey, locationKey }) {
      return new Promise(async (resolve, reject) => {
        await db
          .ref("locations/" + locationKey + "/classes/" + classKey)
          .update(data);
        commit("UPDATE_CLASS", {
          data,
          locationKey
        });
        resolve();
      });
    },
    removeClass({ commit }, { classKey, locationKey }) {
      return new Promise(async (resolve, reject) => {
        await db
          .ref("locations/" + locationKey + "/classes/" + classKey)
          .remove();
        commit("REMOVE_CLASS", {
          classKey,
          locationKey
        });
        resolve();
      });
    }
  }
};
