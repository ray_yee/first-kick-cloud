export const formConfig = [
  {
    label: "Class Name",
    fieldName: "name",
    type: "Input"
  },
  {
    label: "Class Size",
    fieldName: "classSize",
    type: "Number"
  },
  {
    label: "Age Group",
    fieldName: "ageGroup",
    type: "Input"
  },
  {
    label: "Minimum Age",
    fieldName: "minAge",
    type: "Number"
  },
  {
    label: "Maximum Age",
    fieldName: "maxAge",
    type: "Number"
  },
  {
    label: "Day of Week",
    fieldName: "day",
    type: "Select",
    listName: null,
    values: [
      "Saturday",
      "Sunday",
      "Monday",
      "Tuesday",
      "Wednesday",
      "Thursday",
      "Friday"
    ]
  },
  {
    label: "Start Time",
    fieldName: "startTime",
    type: "TimePicker"
  },
  {
    label: "End Time",
    fieldName: "endTime",
    type: "TimePicker"
  },

  {
    label: "Active",
    fieldName: "active",
    type: "Toggle"
  },
  {
    label: "Selected Calendar",
    fieldName: "calendarKey",
    type: "Select",
    listName: "calendars",
    value: "key",
    title: "name"
  },
  {
    label: "Fees Type",
    fieldName: "feesKey",
    type: "Select",
    listName: "fees",
    value: "key",
    title: "name"
  }
];
