import { fs } from "../boot/firebase";

export default {
  namespaced: true,

  state: {
    list: [],
    listByKey: null
  },

  mutations: {
    SET_ORDERS(state, payload) {
      state.listByKey = payload;
      let orders = [];
      Object.keys(payload).forEach(key => {
        orders.push({
          ...payload[key],
          key
        });
      });
      state.list = orders;
    }
  },

  actions: {
    loadOrders({ commit }) {
      return new Promise((resolve, reject) => {
        fs.collection("paymentAdvices")
          .where("type", "in", ["Order Kit", "Registration Fee"])
          .onSnapshot(querySnapshot => {
            let orders = {};
            querySnapshot.forEach(function(doc) {
              orders[doc.id] = {
                ...doc.data()
              };
            });
            commit("SET_ORDERS", orders);
            resolve(orders);
          });
      });
    },
    updateOrder({ commit }, { key, data }) {
      return new Promise(async (resolve, reject) => {
        await fs
          .collection("paymentAdvices")
          .doc(key)
          .update(data);
        resolve();
      });
    }
  }
};
