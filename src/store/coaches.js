import { db } from "../boot/firebase";

export default {
  namespaced: true,

  state: {
    list: null,
    listByKey: null
  },

  mutations: {
    SET_COACHES(state, payload) {
      state.listByKey = payload;
      let coaches = [];
              Object.keys(payload).forEach(key => {
                coaches.push({
                  ...payload[key],
                  key
                });
              });
      
      state.list = coaches;
    }
  },

  actions: {
    loadCoaches({ commit }) {
      return new Promise((resolve, reject) => {
        db.ref("coaches")
          .on("value", snapshot => {
            let val = snapshot.val();
            if (val) {
              
              commit("SET_COACHES", val);
              resolve(val);
            }
            resolve();
          })
      });
    },
    addCoach({ commit }, data) {
      return new Promise(async (resolve, reject) => {
        const newRef = await db.ref("coaches").push(data);
        resolve(newRef.key);
      });
    },
    updateCoach({ commit }, { key, data }) {
      return new Promise(async (resolve, reject) => {
        await db
          .ref("coaches")
          .child(key)
          .update(data);

        resolve();
      });
    }
  }
};
