import { db } from "../boot/firebase";

export default {
  namespaced: true,

  state: {
    list: null,
    all: [],
    allByKey: null,
    archived: [],
    listByKey: null
  },
  getters: {
    students(state) {
      return state.students;
    }
  },

  mutations: {
    SET_STUDENTS(state, payload) {
      let students = [];
      let studentsByKey = {}
      if (payload)
        Object.keys(payload).forEach(key => {
          if (payload[key].status !== "Not Active") {
            const data = {
              ...payload[key],
              key
            }
              studentsByKey[key] = {
                ...data
              }
              students.push({...data});
          }
          
        });
      state.listByKey = studentsByKey
      state.list = students;
    },
    SET_ARCHIVED(state, payload) {
      state.archived = payload;
    },
    SET_ALL_STUDENTS(state, payload) {
      let students = [];
      let studentsByKey = {}

      Object.keys(payload).forEach(key => {
        if (payload[key].status !== "Not Active")
          {const data = {
            ...payload[key],
            key
          }
            studentsByKey[key] = {
              ...data
            }
            students.push({...data});}
      });
      state.allByKey = studentsByKey;
      state.all = students;
    }
  },

  actions: {
    loadAllStudents({ commit }) {
      return new Promise((resolve, reject) => {
        db.ref("students").on("value", snapshot => {
          let val = snapshot.val();
          if (val) {
            commit("SET_ALL_STUDENTS", val);
            resolve(val);
          }
          resolve();
        });
      });
    },
    loadStudents({ commit }, location) {
      return new Promise((resolve, reject) => {
        db.ref("students")
          .orderByChild("assignedLocation")
          .equalTo(location)
          .on("value", snapshot => {
            let val = snapshot.val();
            commit("SET_STUDENTS", val);
            resolve(val);
          });
      });
    },
    loadArchivedStudents({ commit }, locationId) {
      return new Promise((resolve, reject) => {
        db.ref("students")
          .orderByChild("venueId")
          .equalTo(locationId)
          .on("value", snapshot => {
            let val = snapshot.val();
            if (val) {
              let students = [];
              Object.keys(val).forEach(key => {
                if (
                  val[key].status === "Not Active" ||
                  val[key].currentClassTime === "0"
                )
                  students.push({
                    ...val[key],
                    key
                  });
              });
              commit("SET_ARCHIVED", students);
              resolve(students);
            }
            resolve();
          });
      });
    },

    addStudent({ commit }, data) {
      return new Promise(async (resolve, reject) => {
        delete data.key
        const newRef = await db.ref("students").push(data);
        resolve(newRef.key);
      });
    },
    updateStudent({ commit }, { key, data }) {
      return new Promise(async (resolve, reject) => {
        await db
          .ref("students")
          .child(key)
          .update(data);

        resolve();
      });
    }
  }
};
