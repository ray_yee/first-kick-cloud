import { db } from "../boot/firebase";
import moment from "moment";

function getAge(dob) {
  const now = moment();
  const dateofbirth = moment(dob, "YYYY-MM-DD");
  return now.diff(dateofbirth, "years");
}

export default {
  namespaced: true,

  state: {
    trials: {}
  },

  getters: {
    trials(state) {
      return state.trials;
    }
  },

  mutations: {
    SET_TRIALS(state, payload) {
      state.trials = payload;
    },
    UPDATE_TRIAL(state, { key, data }) {
      state.trials = {
        ...state.trials,
        [key]: {
          ...state.trials[key],
          ...data
        }
      };
    }
  },

  actions: {
    loadTrials({ commit }, location) {
      const { id, classes } = location;
      return new Promise((resolve, reject) => {
        db.ref("trials")
          .orderByChild("venueId")
          .equalTo(id)
          .limitToLast(50)
          .once("value", snapshot => {
            let val = snapshot.val();
            if (val) {
              let newTrials = {};
              Object.keys(val).forEach(key => {
                const { dateOfBirth, className } = val[key];
                const age = getAge(dateOfBirth);
                const foundClass = find(
                  classes,
                  c => age <= c.maxAge && age >= c.minAge
                );
                newTrials[key] = {
                  ...val[key],
                  key,
                  age: age ? age : null,
                  className: className
                    ? className
                    : foundClass
                    ? foundClass.name
                    : null
                };
              });
              commit("SET_TRIALS", newTrials);
              resolve(newTrials);
            }
            resolve();
          });
      });
    },
    updateTrialDetails({ commit }, { key, data }) {
      return new Promise((resolve, reject) => {
        db.ref("trials")
          .child(key)
          .update(data);
        commit("UPDATE_TRIAL", { key, data });
        resolve();
      });
    }
  }
};
