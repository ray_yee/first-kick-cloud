import { auth, db } from "../boot/firebase";

export default {
  namespaced: true,

  state: {
    user: null
  },

  getters: {
    user(state) {
      return state.user;
    },

    isAuthenticated(state) {
      return !!state.user;
    }
  },

  mutations: {
    SET_USER(state, payload) {
      let user = payload;
      state.user = user;
    },

    RESET_USER(state) {
      state.user = null;
    }
  },

  actions: {
    async signIn({ dispatch }, payload) {
      let email = payload.email;
      let password = payload.password;

      await auth
        .signInWithEmailAndPassword(email, password)
        .then(user => {
          dispatch("loadUser", user.email);
        })
        .catch(error => {
          throw error;
        });
    },

    async signOut({ commit }) {
      await auth.signOut().then(() => {
        commit("SET_USER", null);
      });
    },

    loadUser({ commit }, email) {
      return new Promise((resolve, reject) => {
        db.ref("users")
          .orderByChild("email")
          .equalTo(email)
          .once("value", snapshot => {
            const fbUser = snapshot.val();
            if (fbUser) {
              const user = Object.keys(fbUser).map(key => {
                return {
                  ...fbUser[key],
                  key
                };
              })[0];
              commit("SET_USER", user);
              resolve(user);
            }
            resolve();
          })
          .catch(err => {
            reject(err);
            console.log("Error getting documents: ", error);
          });
      });
    }
  }
};
