import { fs } from "../boot/firebase";
import groupBy from "lodash/groupBy";
import orderBy from "lodash/orderBy";
import moment from "moment";
export default {
  namespaced: true,

  state: {
    list: null,
    all: null,
    fsList: null,
    listByKey: null
  },

  mutations: {
    SET_CLASSES(state, { classes, classesByKey }) {
      let cla = orderBy(classes, o =>
        moment(o.startTime, "h:mma").format("HH")
      );
      state.list = cla;
      state.listByKey = classesByKey;
    },
    SET_FS_CLASSES(state, payload) {
      state.fsList = payload;
    },
    SET_ALL_CLASSES(state, payload) {
      let classes = orderBy(payload, o =>
        moment(o.startTime, "h:mma").format("HH")
      );
      state.all = groupBy(classes, "locationName");
    }
  },

  actions: {
    loadAllClasses({ commit }) {
      return new Promise((resolve, reject) => {
        fs.collection("classes")
          .where("active", "==", true)
          .onSnapshot(
            function(querySnapshot) {
              let classes = [];
              querySnapshot.forEach(function(doc) {
                classes.push({
                  ...doc.data(),
                  key: doc.id
                });
              });
              commit("SET_ALL_CLASSES", classes);
              resolve();
            },
            error => {
              console.log(error);
              resolve();
            }
          );
      });
    },
    loadClasses({ commit }, locationName) {
      return new Promise((resolve, reject) => {
        fs.collection("classes")
          .where("locationName", "==", locationName)
          .where("active", "==", true)
          .onSnapshot(
            function(querySnapshot) {
              let classes = [];
              let classesByKey = {};
              querySnapshot.forEach(function(doc) {
                classesByKey[doc.id] = {
                  ...doc.data(),
                  key: doc.id
                };
                classes.push({
                  ...doc.data(),
                  key: doc.id
                });
              });
              commit("SET_CLASSES", { classes, classesByKey });
              resolve();
            },
            error => {
              console.log(error);
              resolve();
            }
          );
      });
    },
    addClass({ commit }, data) {
      return new Promise(async (resolve, reject) => {
        fs.collection("classes")
          .add(data)
          .then(function() {
            console.log("Document successfully written!");
            resolve();
          })
          .catch(function(error) {
            console.log(data);
            console.error("Error writing document: ", error);

            reject(error);
          });
      });
    },
    updateClass({ commit }, { data, classKey }) {
      return new Promise(async (resolve, reject) => {
        fs.collection("classes")
          .doc(classKey)
          .update(data)
          .then(function() {
            console.log("Document successfully written!");
            resolve();
          })
          .catch(function(error) {
            console.log(data);
            console.error("Error writing document: ", error);

            reject(error);
          });
      });
    },
    removeClass({ commit }, classKey) {
      return new Promise(async (resolve, reject) => {
        fs.collection("classes")
          .doc(classKey)
          .delete()
          .then(function() {
            console.log("Document successfully written!");
            resolve();
          })
          .catch(function(error) {
            console.log(data);
            console.error("Error writing document: ", error);

            reject(error);
          });
      });
    },
    copyClasses({ commit }, data) {
      return new Promise((resolve, reject) => {
        fs.collection("classes")
          .add(data)
          .then(function() {
            console.log("Document successfully written!");
            resolve();
          })
          .catch(function(error) {
            console.log(data);
            console.error("Error writing document: ", error);

            reject(error);
          });
      });
    }
  }
};
