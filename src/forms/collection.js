export const formConfig = [
    {
        label: "Date of Collection",
        fieldName: "dateOfCollection",
        type: "DatePicker"

      },

    {
        label: "Collection Time",
        fieldName: "collectionTime",
        type: "TimePicker"
      },
  ];
  