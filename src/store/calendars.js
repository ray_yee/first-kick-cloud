import { db } from "../boot/firebase";
import moment from "moment";

export default {
  namespaced: true,

  state: {
    termDatesDetails: null,
    calendars: null,
    termDates: null,
    selectedCalendarDetails: null
  },

  mutations: {
    SET_CALENDAR(state, { termDates, calendars }) {
      state.calendars = calendars;
      state.termDatesDetails = termDates;
      state.termDates = calendars ? Object.keys(termDates) : null;
    },
    SET_CALENDAR_TERMS(state, payload) {
      state.selectedCalendarDetails = payload;
    },
    ADD_CALENDAR(state, data) {
      // let termDates = {}
      const { key } = data;
      state.calendars = {
        ...state.calendars,
        [key]: {
          ...data
        }
      };
      // Object.keys(terms).forEach(yearKey => {
      //   if (yearKey === currentYear) {
      //     const year = terms[yearKey];
      //     Object.keys(year).forEach(key => {
      //       const dates = year[key];
      //       dates.forEach(date => {
      //         termDates[moment(date).format("DD-MM-YYYY")] = {
      //           term: key,
      //           year: moment(date).format("YYYY")
      //         };
      //       });
      //     });
      //   }
      // })
    },
    UPDATE_CALENDAR(state, { data, calendarKey }) {
      state.calendars = Object.keys(state.calendars).map(key => {
        if (key === calendarKey) {
          return {
            ...state.calendars[key],
            ...data
          };
        } else {
          return state.calendars[key];
        }
      });
    }
  },

  actions: {
    loadAllCalendars({commit}) {
      return new Promise((resolve, reject) => {
        db.ref("calendars")
          .once("value", snapshot => {
            let val = snapshot.val();
            if (val) {
              const newVal = Object.keys(val).map(key => {
                return {
                  ...val[key],
                  key
                };
              });
              let termDates = {};
              const currentYear = moment().format("YYYY");
              newVal.forEach(calendar => {
                const { terms } = calendar;
                if (terms) {
                  Object.keys(terms).forEach(yearKey => {
  
                      const year = terms[yearKey];
                      Object.keys(year).forEach(key => {
                        const dates = year[key];
                        dates.forEach(date => {
                          termDates[moment(date).format("DD-MM-YYYY")] = {
                            term: key,
                            year: moment(date).format("YYYY")
                          };
                        });
                      });
                    
                  });
                }
              });
              commit("SET_CALENDAR", { termDates, calendars: val });
              resolve({ termDates, calendars: val });
            } else {
              commit("SET_CALENDAR", { termDates: null, calendars: null });
              resolve({ termDates: null, calendars: null });
            }
            resolve();
          })
          .catch(function(error) {
            reject();
            console.log("Error getting documents: ", error);
          });
      });
    },
    loadCalendar({ commit }, location) {
      return new Promise((resolve, reject) => {
        db.ref("calendars")
          .orderByChild("centreKey")
          .equalTo(location.key)
          .once("value", snapshot => {
            let val = snapshot.val();
            if (val) {
              const newVal = Object.keys(val).map(key => {
                return {
                  ...val[key],
                  key
                };
              });
              let termDates = {};
              newVal.forEach(calendar => {
                const { terms } = calendar;
                if (terms) {
                  Object.keys(terms).forEach(yearKey => {
                      const year = terms[yearKey];
                      Object.keys(year).forEach(key => {
                        const dates = year[key];
                        dates.forEach(date => {
                          termDates[moment(date).format("DD-MM-YYYY")] = {
                            term: key,
                            year: moment(date).format("YYYY")
                          };
                        });
                      });
                    
                  });
                }
              });
              commit("SET_CALENDAR", { termDates, calendars: val });
              resolve({ termDates, calendars: val });
            } else {
              commit("SET_CALENDAR", { termDates: null, calendars: null });
              resolve({ termDates: null, calendars: null });
            }
            resolve();
          })
          .catch(function(error) {
            reject();
            console.log("Error getting documents: ", error);
          });
      });
    },
    getCalendar({ commit }, calendarKey) {
      return new Promise((resolve, reject) => {
        db.ref("calendars/" + calendarKey)
          .once("value", snapshot => {
            let val = snapshot.val();
            if (val) {
              const currentYear = moment().format("YYYY");
              const terms = val.terms[currentYear];
              let termDates = [];
              if (terms === undefined) {
              }
              Object.keys(terms).map(key => {
                const term = terms[key];
                term.map(date =>
                  termDates.push(moment(date).format("DD-MM-YYYY"))
                );
              });
              commit("SET_CALENDAR_TERMS", {
                terms,
                year: currentYear,
                termDates
              });
            }

            resolve();
          })
          .catch(function(error) {
            reject();
            console.log("Error getting documents: ", error);
          });
      });
    },
    addCalendar({ commit }, data) {
      return new Promise(async (resolve, reject) => {
        const newRef = await db.ref("calendars").push(data);
        commit("ADD_CALENDAR", { ...data, key: newRef.key });
        resolve();
      });
    },
    updateCalendar({ commit }, { data, calendarKey }) {
      return new Promise(async (resolve, reject) => {
        await db.ref("calendars/" + calendarKey).update(data);
        commit("UPDATE_CALENDAR", { data, calendarKey });
        resolve();
      });
    }
  }
};
