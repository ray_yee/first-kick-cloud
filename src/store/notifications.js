import { fs } from "../boot/firebase";

export default {
  namespaced: true,

  state: {
    list: [],
    listByKey: null
  },

  actions: {
    addNotifications({ commit }, { tokens, data }) {
      return new Promise(async (resolve, reject) => {
        const batch = fs.batch();
        tokens.forEach(token => {
          let newRef = fs.collection("notifications").doc();
          batch.set(newRef, { ...data, token });
        });
        batch.commit().then(() => {
          resolve();
        });
      });
    }
  }
};
