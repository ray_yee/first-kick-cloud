export const formConfig = [
  
    {
      label: "Child's Name",
      fieldName: "childName",
      type: "Input"
    },
    {
      label: "Date of Birth (YYYY-MM-DD)",
      fieldName: "dateOfBirth",
      type: "DatePicker"
    },
    {
      label: "Medical Condition",
      fieldName: "medicalCondition",
      type: "Input",
      required: false
    },
    {
      label: "Parent's Name",
      fieldName: "parentName",
      type: "Input"
    },
    {
      label: "Email",
      fieldName: "email",
      type: "Input"
    },
    {
      label: "Mobile Number",
      fieldName: "contact",
      type: "Input"
    }
  ];
  