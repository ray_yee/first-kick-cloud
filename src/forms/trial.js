export const formConfig = [
  {
    label: "Selected Location",
    fieldName: "venueId",
    type: "Select",
    listName: "locations",
    value: "id",
    title: "name"
  },
  {
    label: "Selected Class Time",
    fieldName: "className",
    type: "Select",
    listName: "classes",
    value: "name",
    title: "name"
  },
  {
    label: "Selected Date Of Trial",
    fieldName: "dateOfTrial",
    type: "Select",
    listName: "termDates",
    value: "id",
    title: "name"
  },
  {
    label: "Child's Name",
    fieldName: "childName",
    type: "Input"
  },
  {
    label: "Date of Birth (YYYY-MM-DD)",
    fieldName: "dateOfBirth",
    type: "DatePicker"
  },
  {
    label: "Medical Condition",
    fieldName: "medicalCondition",
    type: "Input",
    required: false
  },
  {
    label: "Parent's Name",
    fieldName: "parentName",
    type: "Input"
  },
  {
    label: "Email",
    fieldName: "email",
    type: "Input"
  },
  {
    label: "Mobile Number",
    fieldName: "contact",
    type: "Input"
  }
];
