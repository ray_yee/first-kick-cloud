import { db } from "../boot/firebase";
import orderBy from "lodash/orderBy";
export default {
  namespaced: true,

  state: {
    ccleads: [],
    ccleadsByKey: null
  },

  mutations: {
    SET_CCLEADS(state, payload) {
      let ccleads = [];
      Object.keys(payload).forEach(key => {
        ccleads.push({
          ...payload[key],
          key
        });
      });
      state.ccleadsByKey = payload;
      state.ccleads = orderBy(ccleads, "dateAdded", "desc");
    }
  },

  actions: {
    loadccLeads({ commit }) {
      return new Promise((resolve, reject) => {
        db.ref("ccleads").on("value", snapshot => {
          let val = snapshot.val();
          if (val) {
            commit("SET_CCLEADS", val);
            resolve(val);
          }
          resolve();
        });
      });
    },

    addLead({ commit }, leadData) {
      return new Promise(async (resolve, reject) => {
        const newRef = await db.ref("leads").push(leadData);
        // commit("ADD_LEAD", { ...leadData, key: newRef.key });
        resolve();
      });
    },
    updateLead({ commit }, { key, data }) {
      return new Promise(async (resolve, reject) => {
        await db
          .ref("leads")
          .child(key)
          .update(data);

        resolve();
      });
    },
    deleteLead({ commit }, key) {
      return new Promise(async (resolve, reject) => {
        await db
          .ref("ccleads")
          .child(key)
          .remove();

        resolve();
      });
    }
  }
};
