import Firebase from "firebase/app";
import "firebase/auth";

export default ({ app, router, Vue, store }) => {
  // Register the Firebase authentication listener
  Firebase.auth().onAuthStateChanged(async user => {
    if (user) {
      const authUser = await store.dispatch("auth/loadUser", user.email);
      // Signed in. Let Vuex know.
      // store.commit("auth/SET_USER", user);
      await store.dispatch("locations/loadLocations", authUser.assignedCentres);
      router.replace({ name: router.currentRoute.name }).catch(() => {});
      new Vue(app); /* eslint-disable-line no-new */
    } else {
      // Signed out. Let Vuex know.
      store.commit("auth/RESET_USER");
      router.replace({ name: "login" }).catch(() => {});
      new Vue(app); /* eslint-disable-line no-new */
    }
  });
};
