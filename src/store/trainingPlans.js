import { fs } from "../boot/firebase";
import groupBy from "lodash/groupBy";

export default {
  namespaced: true,

  state: {
    listByMonth: null
  },

  mutations: {
    SET_TRAINING_PLANS(state, payload) {
      state.listByMonth = groupBy(payload, "month");
    }
  },

  actions: {
    loadTrainingPlans({ commit }, year) {
      return new Promise((resolve, reject) => {
        fs.collection("trainingPlans")
          .where("year", "==", year)
          .onSnapshot(querySnapshot => {
            let plans = [];
            querySnapshot.forEach(function(doc) {
              plans.push({
                key: doc.id,
                ...doc.data()
              });
            });
            commit("SET_TRAINING_PLANS", plans);
            resolve(plans);
          });
      });
    },
    addTrainingPlans({ commit }, { plans, month, year }) {
      return new Promise(async (resolve, reject) => {
        const batch = fs.batch();
        plans.forEach(plan => {
          let newRef = fs.collection("trainingPlans").doc();
          batch.set(newRef, { ...plan, month, year });
        });
        batch.commit().then(() => {
          resolve();
        });
      });
    },
    updateTrainingPlan({ commit }, { key, data }) {
      return fs
        .collection("trainingPlans")
        .doc(key)
        .update({ ...data });
    },
    deleteTrainingPlan({ commit }, key) {
      return fs
        .collection("trainingPlans")
        .doc(key)
        .delete();
    }
  }
};
